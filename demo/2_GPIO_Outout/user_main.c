/*
 * @Author: your name
 * @Date: 2020-05-19 14:05:32
 * @LastEditTime: 2020-05-19 17:34:30
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\USER\user_main.c
 */
#include "osi_log.h"
#include "osi_api.h"

#include "string.h"
#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_os.h"
#include "iot_gpio.h"

static void GPIO0(void *param)
{
	T_AMOPENAT_GPIO_CFG output_cfg = {0};
	output_cfg.mode = OPENAT_GPIO_OUTPUT; //配置输出
	output_cfg.param.defaultState = 0;	  // 默认低电平
	BOOL err = 0;
	// gpio0初始化
	err = iot_gpio_config(0, &output_cfg);
	if (!err)
		return;
	while (1)
	{
		//将GPIO0设置为高电平
		iot_gpio_set(0, 1); //设置为高电平
		iot_debug_print("GPIO0 :%d", 1);
		//线程休眠500ms
		osiThreadSleep(500);
		//将GPIO0设置为低电平
		iot_gpio_set(0, 0); //设置为高电平
		iot_debug_print("GPIO0 :%d", 0);
		//线程休眠500ms
		osiThreadSleep(500);
	}
	osiThreadExit();
}
static void GPIO1(void *param)
{
	T_AMOPENAT_GPIO_CFG output_cfg = {0};
	output_cfg.mode = OPENAT_GPIO_OUTPUT; //配置输出
	output_cfg.param.defaultState = 0;	  // 默认低电平
	BOOL err = 0;
	// gpio1初始化
	err = iot_gpio_config(1, &output_cfg);
	if (!err)
		return;
	while (1)
	{
		//将GPIO1设置为高电平
		iot_gpio_set(1, 1); //设置为高电平
		iot_debug_print("GPIO1 :%d", 1);
		//线程休眠1000ms
		osiThreadSleep(1000);
		//将GPIO1设置为低电平
		iot_gpio_set(1, 0); //设置为高电平
		iot_debug_print("GPIO1 :%d", 0);
		//线程休眠1000ms
		osiThreadSleep(1000);
	}
	osiThreadExit();
}
//main函数
int appimg_enter(void *param)
{
	//创建一个任务
	osiThreadCreate("GPIO0", GPIO0, NULL, OSI_PRIORITY_NORMAL, 1024, 0);
	//再创建一个任务
	osiThreadCreate("GPIO1", GPIO1, NULL, OSI_PRIORITY_NORMAL, 1024, 0);
	return 0;
}

//退出提示
void appimg_exit(void)
{
	OSI_LOGI(0, "application image exit");
}
