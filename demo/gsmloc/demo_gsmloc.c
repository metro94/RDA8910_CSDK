#include "iot_gsmloc.h"
#include "iot_os.h"
#include "iot_debug.h"

#define demo_gsmloc_print iot_debug_print
extern int gsmloc_reagyflg;

VOID demo_gsmloc(VOID)
{	
	u8 buf[100] = {0};
	while(1)
	{
		if(gsmloc_reagyflg == 1)
		{
			iot_get_Gsmloc(buf);
			demo_gsmloc_print("[gsmloc]info: %s", buf);
			break;
		}
		iot_os_sleep(1000);
	}	
}

int appimg_enter(void *param)
{
    iot_os_sleep(400);
	demo_gsmloc_print("[gsmloc] app_main");
	iot_gsmloc_init();
	demo_gsmloc();
    return 0;
}

void appimg_exit(void)
{
    demo_gsmloc_print("[gsmloc] application image exit");
}

