/*
 * @Author: your name
 * @Date: 2020-05-18 19:45:54
 * @LastEditTime: 2020-05-20 15:17:29
 * @LastEditors: your name
 * @Description: In User Settings Edit
 * @FilePath: \RDA8910_CSDK\api\src\iot_adc.c
 */ 
#include "iot_adc.h"
#include "am_openat.h"
/**ADC初始化 
*@param		chanle:		adc通道
*@return	TRUE: 	    成功
*           FALSE:      失败
**/
BOOL iot_adc_init(
                        E_AMOPENAT_ADC_CHANNEL chanle
                )
{
    return OPENAT_init_adc(chanle);
}

/**读取ADC数据
*@note ADC值，可以为空, 电压值，可以为空
*@param		chanle:		adc通道
*@param		adcValue:	ADC值，可以为空
*@param		voltage:	电压值，可以为空
*@return	TRUE: 	    成功
*           FALSE:      失败
**/
BOOL iot_adc_read(
                        E_AMOPENAT_ADC_CHANNEL chanle,     
                        UINT32* adcValue,                  
                        UINT32* voltage                   
                )
{
    return OPENAT_read_adc(chanle, adcValue, voltage);
}
