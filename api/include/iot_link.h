#ifndef __IOT_LINK_H__
#define __IOT_LINK_H__

#include "iot_debug.h"
#include "iot_network.h"

BOOL network_connect(T_OPENAT_NETWORK_CONNECT* connectParam);
BOOL network_disconnect(BOOL flymode);
BOOL network_get_status(T_OPENAT_NETWORK_STATUS* status);
BOOL network_set_cb(F_OPENAT_NETWORK_IND_CB indCb);

#endif
